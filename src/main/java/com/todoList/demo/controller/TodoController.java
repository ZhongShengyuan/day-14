package com.todoList.demo.controller;

import com.todoList.demo.entity.Todo;
import com.todoList.demo.service.TodoService;
import com.todoList.demo.service.dto.CreateTodoRequest;
import com.todoList.demo.service.dto.UpdateTodoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
@CrossOrigin
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping("")
    public List<Todo> getTodoList(){
        return todoService.getTodoList();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo createTodo(@RequestBody CreateTodoRequest createTodoRequest){
        return todoService.createTodo(createTodoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id){
        todoService.deletTodo(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Todo updateTodo(@PathVariable Long id, @RequestBody UpdateTodoRequest updateTodoRequest){
        Todo todo = todoService.updateTodo(id, updateTodoRequest);
        return todo;
    }

    @GetMapping("/{id}")
    public Todo getTodoById(@PathVariable Long id){
        Todo todo = todoService.getTodoById(id);
        return todo;
    }

}
