package com.todoList.demo.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Data
@Entity
@Table(name = "todo")
@DynamicInsert
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    private Boolean done;

    public Todo(String text) {
        this.text = text;
    }

    public Todo() {
    }
}
