package com.todoList.demo.repository;

import com.todoList.demo.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPATodoRepository extends JpaRepository<Todo, Long> {
}
