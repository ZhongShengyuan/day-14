package com.todoList.demo.service;

import com.todoList.demo.entity.Todo;
import com.todoList.demo.exceptions.TodoNotFoundException;
import com.todoList.demo.repository.JPATodoRepository;
import com.todoList.demo.service.dto.CreateTodoRequest;
import com.todoList.demo.service.dto.UpdateTodoRequest;
import com.todoList.demo.service.mapper.TodoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    @Autowired
    private JPATodoRepository jpaTodoRepository;

    public List<Todo> getTodoList() {
        return jpaTodoRepository.findAll();
    }

    public Todo createTodo(CreateTodoRequest createTodoRequest) {
        Todo todo = TodoMapper.toEntity(createTodoRequest);
        return jpaTodoRepository.save(todo);
    }

    public void deletTodo(Long id) {
        jpaTodoRepository.deleteById(id);
    }

    public Todo updateTodo(Long id, UpdateTodoRequest updateTodoRequest) {
        Todo todo = jpaTodoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (updateTodoRequest.getDone() != null) {
            todo.setDone(updateTodoRequest.getDone());
        }
        if (updateTodoRequest.getText() != null) {
            todo.setText(updateTodoRequest.getText());
        }
        return jpaTodoRepository.save(todo);
    }

    public Todo getTodoById(Long id) {
        return jpaTodoRepository.findById(id).orElseThrow();
    }
}
