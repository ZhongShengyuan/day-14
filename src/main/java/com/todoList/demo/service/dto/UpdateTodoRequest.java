package com.todoList.demo.service.dto;

import lombok.Data;

@Data
public class UpdateTodoRequest {

    private String text;

    private Boolean done;
}
