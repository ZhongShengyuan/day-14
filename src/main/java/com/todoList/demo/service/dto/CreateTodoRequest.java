package com.todoList.demo.service.dto;

import lombok.Data;

@Data
public class CreateTodoRequest {

    private String text;
}
