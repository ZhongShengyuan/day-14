package com.todoList.demo.service.mapper;

import com.todoList.demo.entity.Todo;
import com.todoList.demo.service.dto.CreateTodoRequest;
import com.todoList.demo.service.dto.UpdateTodoRequest;
import org.springframework.beans.BeanUtils;

public class TodoMapper {

    public static Todo toEntity(CreateTodoRequest createTodoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(createTodoRequest, todo);
        return todo;
    }

    public static Todo toEntity(UpdateTodoRequest updateTodoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(updateTodoRequest, todo);
        return todo;
    }
}
