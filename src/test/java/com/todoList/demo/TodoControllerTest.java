package com.todoList.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoList.demo.entity.Todo;
import com.todoList.demo.repository.JPATodoRepository;
import com.todoList.demo.service.dto.CreateTodoRequest;
import com.todoList.demo.service.dto.UpdateTodoRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @BeforeEach
    void before(){
        jpaTodoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception{
        Todo todo = getTodo1();
        jpaTodoRepository.save(todo);

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()));
    }

    @Test
    void should_create_todo() throws Exception {
        CreateTodoRequest createTodoRequest1 = getCreateTodoRequest1();

        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(createTodoRequest1);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(createTodoRequest1.getText()));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo("abc");
        jpaTodoRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertFalse(jpaTodoRepository.findById(todo.getId()).isPresent());
    }

    @Test
    void should_update_todo() throws Exception {
        Todo previousTodo = new Todo("abc");
        Todo saveTodo = jpaTodoRepository.save(previousTodo);

        UpdateTodoRequest updateTodoRequest1 = getUpdateTodoRequest1();
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(updateTodoRequest1);
        mockMvc.perform(put("/todos/{id}", saveTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Todo> optionalTodo = jpaTodoRepository.findById(saveTodo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updateTodo = optionalTodo.get();
        Assertions.assertEquals(saveTodo.getId(), updateTodo.getId());
        Assertions.assertEquals(updateTodoRequest1.getText(), updateTodo.getText());
    }

    @Test
    void should_find_todo_when_done_is_false_by_id() throws Exception {
        Todo todo1 = getTodo1();
        Todo saveTodo1 = jpaTodoRepository.save(todo1);

        mockMvc.perform(get("/todos/{id}", saveTodo1.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo1.getDone()));
    }


    private static Todo getTodo1() {
        Todo todo = new Todo();
        todo.setText("ABC");
        return todo;
    }

    private static CreateTodoRequest getCreateTodoRequest1() {
        CreateTodoRequest createTodoRequest = new CreateTodoRequest();
        createTodoRequest.setText("ABC");
        return createTodoRequest;
    }

    private static UpdateTodoRequest getUpdateTodoRequest1(){
        UpdateTodoRequest updateTodoRequest = new UpdateTodoRequest();
        updateTodoRequest.setDone(true);
        updateTodoRequest.setText("ABC");
        return updateTodoRequest;
    }
}
